// @(#)root/net:$Id$
// Author: Fons Rademakers   3/1/2002

/*************************************************************************
 * Copyright (C) 1995-2002, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

#ifndef ROOT_TJAlien
#define ROOT_TJAlien
#include <cstdio>
#include <unistd.h>
#include <map>
#include <fstream>
#include <cstdlib>
#include <string>
#include <csignal>
#include <sstream>

#ifdef _DEBUG
#define DEBUGMSG(msg) ( (void) (std::cerr<<msg<<std::endl) )
#else
#define DEBUGMSG(msg) ( (void) 0 )
#endif
#define CMDEND '\0'
#define CMDOPTIONS '\1'

#ifndef ROOT_TGrid
#include "TGrid.h"
#endif

#ifndef ROOT_TList
#include "TList.h"
#endif

#include "TArrayI.h"
#include "THashList.h"
#include <TLockFile.h>
#include "TObjArray.h"
#include "TString.h"
#include "TJAlienJDL.h"
#include "TJAlienCollection.h"
#include "TJAlienFile.h"
#include "TJAlienJob.h"
#include "TJAlienJobStatus.h"
#include "TJAlienJobStatusList.h"
#include "TJAlienResultRewriter.h"
#include "TJAlienSAXHandler.h"
#include "TJAlienResult.h"
#include "TJAlienConnectionManager.h"

#if !defined(__CINT__) && !defined(__MAKECINT__) && !defined(__ROOTCLING__) && !defined(__CLING__)
#include <json-c/json.h>
#else
struct json_object;
#endif

#ifdef __CINT__
#undef __GNUC__
typedef char __signed;
typedef char int8_t;
#endif

#if (SSLEAY_VERSION_NUMBER >= 0x0907000L)
#include <openssl/conf.h>
#include <openssl/ssl.h>
#include <openssl/x509.h>
#include <openssl/x509v3.h>
#include <openssl/lhash.h>
#include <openssl/buffer.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#endif

#define UNUSED(x) (void)(x)

class TJAlien : public TGrid {
private:
    static TString fPwd; 	// working directory
    TString fHome;       	// home directory with alien:// prefix
    std::string tmpdir;  	// tmp directory
    std::string readBuffer;	// stdout & stderr of the last command

    Bool_t WriteTokenFile();
    void Connect();

    virtual TGridResult *OpenDataset(const char *lfn, const char *options = "");

    TJAlienConnectionManager connection;

public:
    enum { kSTDOUT = 0, kSTDERR = 1 , kOUTPUT = 2, kENVIR = 3 };
    enum CatalogType { kFailed = -1, kFile = 0, kDirectory, kCollection };

    TJAlien(const char *gridUrl, const char *uId=0, const char *passwd=0,
            const char *options=0);
    virtual ~TJAlien();

    TGridResult *Command(const char * command, bool interactive = kFALSE, UInt_t stream = kOUTPUT);

    Int_t GetExitCode(TJAlienResult *result, TObjString* &message);
    unsigned int ReadTags(int column, std::map<std::string, std::string> &tags) const;

    virtual TGridResult *Query(const char *path, const char *pattern, const char *conditions = "", const char *options = "");

    void Stdout();          // print the stdout of the last executed command
    void Stderr();          // print the stderr of the last executed command

    void Token(Option_t* options = "", bool force_restart = true);
    TGridResult* SetSite(const char *site);
    const char* Whoami();

    //--- Redefinition of superclass methods
    virtual Bool_t IsConnected() const { return connection.IsConnected(); }

    //--- Catalogue Interface
    virtual TGridResult *Ls(const char *ldn = "", Option_t *options = "", Bool_t verbose = kFALSE);
    virtual Bool_t Cd(const char* ldn = "",Bool_t verbose = kFALSE);
    virtual const char *Pwd(Bool_t verbose = kFALSE);
    virtual const char *GetHomeDirectory();
    virtual Int_t  Mkdir(const char* ldn = "", Option_t* options = "", Bool_t verbose = kFALSE);
    virtual Bool_t Rmdir(const char* ldn = "", Option_t* options = "", Bool_t verbose = kFALSE);
    virtual Bool_t Register(const char *lfn, const char *turl, Long_t size=-1, const char *se=0, const char *guid=0, Bool_t verbose=kFALSE);
    virtual Bool_t Rm(const char *lfn, Option_t *option = "", Bool_t verbose = kFALSE);
    virtual TJAlien::CatalogType Type(const char* lfn, Option_t* option = "", Bool_t verbose = kFALSE);
    TGridResult* GetCollection(const char* lfn, Option_t* option = "", Bool_t verbose = kFALSE);

    //--- Job Submission Interface
    virtual TGridJob *Submit(const char * jdl);
    virtual TGridJDL *GetJDLGenerator();        // get a JAliEn grid JDL object
    virtual Bool_t ResubmitById(TString jobid);
    virtual Bool_t KillById(TString jobid);

    virtual TGridJobStatusList *Ps(const char* options = "", Bool_t verbose = kFALSE);
    virtual TGridCollection *OpenCollection(const char *, UInt_t maxentries = 1000000);
    virtual TGridCollection *OpenCollectionQuery(TGridResult *queryresult, Bool_t nogrouping);

    //--- Not implemented staff here
    virtual void NotImplemented(const char *func, const char *file, int line);
    TMap *GetColumn(UInt_t stream = 0, UInt_t column = 0);
    const char *GetStreamFieldValue(UInt_t stream, UInt_t column, UInt_t row);
    const char *GetStreamFieldKey(UInt_t stream, UInt_t column, UInt_t row);
    UInt_t GetNColumns(UInt_t stream);
    virtual TGridResult* ListPackages(const char* alienpackagedir="/alice/packages");

    ClassDef(TJAlien, 0)  // Interface to JAliEn GRID services
};
#endif
