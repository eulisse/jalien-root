#ifndef ROOT_TJAlienSAXHandler
#define ROOT_TJAlienSAXHandler
#include "TSAXParser.h"
#include "TList.h"
#include "TXMLAttr.h"
#include "TJAlienResult.h"
#include "TMap.h"
#define CMDEND '\0'
#define UNUSED(x) (void)(x)
class TJAlienSAXHandler: public TObject {
private:
	TJAlienResult *fJAliEnResult;
public:
   TJAlienSAXHandler(TJAlienResult *result);
   ~TJAlienSAXHandler();

   void     OnStartDocument();
   void     OnEndDocument();
   void     OnStartElement(const char*, const TList*);
   void     OnEndElement(const char*);
   void     OnCharacters(const char*);
   void     OnComment(const char*);
   void     OnWarning(const char*);
   void     OnError(const char*);
   void     OnFatalError(const char*);
   void     OnCdataBlock(const char*, int);

    ClassDef(TJAlienSAXHandler, 0);
};
#endif
