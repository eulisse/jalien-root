#ifndef ROOT_TJAlienDNSResolver
#define ROOT_TJAlienDNSResolver
#include "TObject.h"

#include <string>
#include <vector>
#include <algorithm>
#include <random>
#include <cstring>

#if !defined(__CINT__) && !defined(__MAKECINT__) && !defined(__ROOTCLING__) && !defined(__CLING__)
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#else
struct addrinfo;
#endif

using std::string;
using std::vector;

class TJAlienDNSResolver : public TObject {
 public:
    TJAlienDNSResolver(string host, int port);
    string get_next_host();
    int lenght();
    void reset();

 private:
    string host;
    string port;

    bool use_ipv6;
    int current_position;

    vector<string> addr_ipv4;
    vector<string> addr_ipv6;
    vector<string> addr_combined;
    string addr2string(const struct addrinfo *ai);
    vector<string> get_addr(const char *host, const char *port, int ipv = 6);

    ClassDef(TJAlienDNSResolver, 1)
};

#endif
