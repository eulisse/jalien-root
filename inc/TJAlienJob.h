/*
 * TJAlienJob.h
 *
 *  Created on: Aug 13, 2014
 *      Author: Tatianka Tothova
 */

#ifndef ROOT_TJAlienJob
#define ROOT_TJAlienJob

#ifndef ROOT_TGridJob
#include "TGridJob.h"
#endif


class TJAlienJob : public TGridJob {
public:
   TJAlienJob(TString jobID) : TGridJob(jobID) { }
   virtual ~TJAlienJob() {}

   virtual TGridJobStatus *GetJobStatus() const;
   virtual Bool_t          Resubmit();
   virtual Bool_t          Cancel();

   ClassDef(TJAlienJob,1)  // JAliEn implementation of TGridJob
};

#endif
