// @(#)root/alien:$Id$
// Author: Jan Fiete Grosse-Oetringhaus  27/10/2004

/*************************************************************************
 * Copyright (C) 1995-2004, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

#ifndef ROOT_TJAlienMasterJob
#define ROOT_TJAlienMasterJob

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TJAlienMasterJob                                                      //
//                                                                      //
// Special Grid job which contains a master job which controls          //
// underlying jobs resulting from job splitting.                        //
//                                                                      //
// Related classes are TJAlienJobStatus.                                 //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#ifndef ROOT_TGridJob
#include "TGridJob.h"
#endif


class TJAlienMasterJob : public TGridJob {

public:
    TJAlienMasterJob(TString jobID) : TGridJob(jobID) { }
    virtual ~TJAlienMasterJob() { }

    virtual TGridJobStatus *GetJobStatus() const;

    void   Print(Option_t *) const;
    Bool_t Merge();
    Bool_t Merge(const char *inputname, const char *mergeoutput = 0);
    void   Browse(TBrowser* b);

    ClassDef(TJAlienMasterJob, 1) // Special Alien grid job controlling results of job splitting
};

#endif
