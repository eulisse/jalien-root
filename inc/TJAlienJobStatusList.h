/*
 * TJAlienJobStatusList.h
 *
 *  Created on: Sep 4, 2014
 *      Author: Tatianka Tothova
 */

#ifndef ROOT_TJAlienJobStatusList
#define ROOT_TJAlienJobStatusList

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TJAlienJobStatusList                                                  //
//                                                                      //
// JAliEn implementation of TGridJobStatusList                           //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#ifndef ROOT_TGridJobStatusList
#include "TGridJobStatusList.h"
#endif

class TJAlienJob;
class TJAlienJobStatusList : public TGridJobStatusList {

protected:
   TString  fJobID;  // the job's ID
   virtual void PrintCollectionEntry(TObject* entry, Option_t* option, Int_t recurse) const;

public:
   TJAlienJobStatusList() { gGridJobStatusList = this; };
   virtual ~TJAlienJobStatusList() { if (gGridJobStatusList == this) gGridJobStatusList = 0; };

   ClassDef(TJAlienJobStatusList,1)  // ABC defining interface to a list of JAliEn GRID jobs

};

#endif /* ROOT_TJAlienJobStatusList */
