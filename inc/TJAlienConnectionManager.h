// Author: Volodymyr Yurchenko 27/06/2019

#ifndef ROOT_TJAlienConnectionManager
#define ROOT_TJAlienConnectionManager

#if !defined(__CINT__) && !defined(__MAKECINT__) && !defined(__ROOTCLING__) && !defined(__CLING__)
#include <json-c/json.h>
#else
struct json_object;
#endif

#include <sys/stat.h>
#if !defined(__CINT__) && !defined(__MAKECINT__) && !defined(__ROOTCLING__) && !defined(__CLING__)
#include <libwebsockets.h>
#include "lws_config.h"
#else
struct lws_context;
struct lws_context_creation_info;
#endif

#include "TJAlienResult.h"
#include "TJAlienCredentials.h"
#include "TJAlienDNSResolver.h"
#include "TJClientFile.h"
#include "TError.h"
#include "TGrid.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <fstream>

#define DEFAULT_JCENTRAL_SERVER "alice-jcentral.cern.ch"
#define UNUSED(x) (void)(x)

class TJAlienConnectionManager {
private:
    const int default_WSport = 8097;

    const std::string default_server = DEFAULT_JCENTRAL_SERVER;
    std::string fWSHost;     // websocket host
    int         fWSPort;     // websocket port
    TString     sUsercert;   // location of user certificate
    TString     sUserkey;    // location of user private key

    // Libwebsockets
    static int destroy_flag;                // Flags to know connection status
    static int connection_flag;
    static int writeable_flag;
    static int receive_flag;

    const int default_ping_timeout = 20;
    const int default_ping_interval = 20;

    struct lws_context *context;            // Context contains all information about connection
    /* struct lws_context_creation_info creation_info;	// Info to create logical connection */
    struct lws *wsi;                        // WebSocket Instance - real connection object, created basing on context

    #if !defined(__CINT__) && !defined(__MAKECINT__) && !defined(__ROOTCLING__) && !defined(__CLING__)
    static int ws_service_callback(         // Callback to handle connection
        struct lws *wsi,
        enum lws_callback_reasons reason, void *user,
        void *in, size_t len);
    static int websocket_write_back(struct lws *wsi_in, const char *str, int str_size_in);
    #endif

    static size_t WriteCallback(void *contents, size_t size, size_t nmemb);
    void clearFlags();
    TJAlienCredentials creds;
    static std::string readBuffer;

public:
    TJAlienConnectionManager() {} // default constructor
    ~TJAlienConnectionManager();
    int CreateConnection();
    void ConnectJBox(TJAlienCredentialsObject c);
    void ConnectJCentral(TJAlienCredentialsObject c, string host = DEFAULT_JCENTRAL_SERVER);
    void MakeWebsocketConnection(TJAlienCredentialsObject creds, string host, int WSPort);
    void ForceRestart();
    TJAlienResult *RunJsonCommand(TString *command, TList *options);
    TJAlienResult *RunJsonCommand(TString *command, TList *options, std::map<std::string, TString> *metadata, std::string *readBuffer);

    // Parse the result from Json structure
    TJAlienResult *GetCommandResult(json_object *json_response, bool expand_find = false);

    // Format command to Json structure
    json_object *CreateJsonCommand(TString *command, TList *options);

    virtual Bool_t IsConnected() const;

    ClassDef(TJAlienConnectionManager, 0)
};
#endif
