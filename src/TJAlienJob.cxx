/*
 * TJAlienJob.cxx
 *
 *  Created on: Aug 13, 2014
 *      Author: Tatianka Tothova
 */

#include "TJAlienJob.h"
#include "TGrid.h"
//#include "TJAlienJobStatus.h"
#include "TObjString.h"

ClassImp(TJAlienJob)

//______________________________________________________________________________
TGridJobStatus *TJAlienJob::GetJobStatus() const
{
   // Queries the job for its status and returns a TGridJobStatus object.
   // Returns 0 in case of failure.

	// TODO TODO TODO TODO TODO
/*
   TString jobID;
   jobID = fJobID;

   GAPI_JOBARRAY *gjobarray = gapi_queryjobs("-", "%", "-", "-", "-", "-",
                                             jobID.Data(), "-", "-");

   if (!gjobarray)
      return 0;

   if (gjobarray->size() == 0) {
      delete gjobarray;
      return 0;
   }

   TAlienJobStatus *status = new TAlienJobStatus(); //TODO

   GAPI_JOB gjob = gjobarray->at(0);
   std::map<std::string, std::string>::const_iterator iter = gjob.gapi_jobmap.begin();
   for (; iter != gjob.gapi_jobmap.end(); ++iter) {
      status->fStatus.Add(new TObjString(iter->first.c_str()), new TObjString(iter->second.c_str()));
   }

   delete gjobarray;

   return status;*/
   return NULL;
}

Bool_t TJAlienJob::Cancel()
{
   // Cancels a job e.g. sends a kill command.
   // Returns kFALSE in case of failure, otherwise kTRUE.

   if (gGrid) {
      return gGrid->Kill((TGridJob*)this);
   }
   Error("Cancel","No GRID connection (gGrid=0)");
  return kFALSE;
}

//______________________________________________________________________________
Bool_t TJAlienJob::Resubmit()
{
   // Resubmits a job.
   // Returns kFALSE in case of failure, otherwise kTRUE.

   if (gGrid) {
      return gGrid->Resubmit((TGridJob*)this);
   }
   Error("Cancel","No GRID connection (gGrid=0)");
   return kFALSE;
}
