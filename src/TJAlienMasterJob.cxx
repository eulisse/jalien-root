// @(#)root/alien:$Id$
// Author: Jan Fiete Grosse-Oetringhaus   27/10/2004

/*************************************************************************
 * Copyright (C) 1995-2004, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TJAlienMasterJob                                                      //
//                                                                      //
// Special Grid job which contains a master job which controls          //
// underlying jobs resulting from job splitting.                        //
//                                                                      //
// Related classes are TJAlienJobStatus.                                 //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TJAlienJobStatus.h"
#include "TJAlienJobStatusList.h"
#include "TJAlienMasterJob.h"
#include "TJAlienMasterJobStatus.h"
#include "TJAlienJob.h"
#include "TObjString.h"
#include "Riostream.h"
#include "TGridResult.h"
#include "TJAlien.h"
#include "TFileMerger.h"
#include "TBrowser.h"

ClassImp(TJAlienMasterJob)

//______________________________________________________________________________
void TJAlienMasterJob::Browse(TBrowser* b)
{
    // Browser interface.

    if (b) {
        b->Add(GetJobStatus());
    }
}

//______________________________________________________________________________
TGridJobStatus *TJAlienMasterJob::GetJobStatus() const
{
    // Gets the status of the master job and all its sub jobs.
    // Returns a TJAlienMasterJobStatus object, 0 on failure.

    TString jobID;
    jobID = fJobID;
    
    TString options = TString("- % - - ");
    options += jobID.Data();
    options += "- - - -";

    TJAlienJobStatusList *joblist = (TJAlienJobStatusList*) gGrid->Ps(options.Data());
    TList *list = dynamic_cast < TList * >(joblist);
    TIterator *it = list->MakeIterator();
    
    if (!joblist)
        return 0;

    TJAlienMasterJobStatus *status = new TJAlienMasterJobStatus(fJobID);

    TJAlienJob masterJob(fJobID);
    status->fMasterJob = dynamic_cast<TJAlienJobStatus*>(masterJob.GetJobStatus());
    
    TObject *object = it->Next();
    while (object != 0)
    {
        TMap *statusmap = dynamic_cast < TMap * >(object);
        TJAlienJobStatus *jobStatus = new TJAlienJobStatus(statusmap);
        TObjString* jID = 0;
        jID = (TObjString*) statusmap->GetValue("queueId");
        
        if (jID != 0)
            status->fJobs.Add(jID, jobStatus);
        else
            delete jobStatus;
        
        object = it->Next();
    }
    return status;
}

//______________________________________________________________________________
void TJAlienMasterJob::Print(Option_t* options) const
{
    std::cout << " ------------------------------------------------ " << std::endl;
    std::cout << " Master Job ID                   : " << fJobID << std::endl;
    std::cout << " ------------------------------------------------ " << std::endl;
    TJAlienMasterJobStatus* status = (TJAlienMasterJobStatus*)(GetJobStatus());
    if (!status) {
        Error("Print","Cannot get the information for this masterjob");
        return;
    }

    std::cout << " N of Subjobs                    : " << status->GetNSubJobs() << std::endl;
    std::cout << " % finished                      : " << status->PercentFinished()*100 << std::endl;
    std::cout << " ------------------------------------------------ " << std::endl;
    TIterator* iter = status->GetJobs()->MakeIterator();

    TObjString* obj = 0;
    while ((obj = (TObjString*)iter->Next()) != 0) {
        TJAlienJobStatus* substatus = (TJAlienJobStatus*)status->GetJobs()->GetValue(obj->GetName());
        printf(" SubJob: [%-7s] %-10s %20s@%s  RunTime: %s\n",substatus->GetKey("queueId"),substatus->GetKey("status"),substatus->GetKey("node"),substatus->GetKey("site"),substatus->GetKey("runtime"));
    }
    std::cout << " ------------------------------------------------ " << std::endl;
    iter->Reset();
    if ( strchr(options,'l') ) {
        while ((obj = (TObjString*)iter->Next()) != 0) {
            TJAlienJobStatus* substatus = (TJAlienJobStatus*)status->GetJobs()->GetValue(obj->GetName());
            // list sandboxes
            const char* outputdir = substatus->GetJdlKey("OutputDir");

            TString sandbox;
            if (outputdir) {
                sandbox = outputdir;
            } else {
                sandbox = TString("/proc/") + TString(substatus->GetKey("user")) + TString("/") + TString(substatus->GetKey("queueId")) + TString("/job-output");
            }

            printf(" Sandbox [%-7s] %s \n", substatus->GetKey("queueId"),sandbox.Data());
            std::cout << " ================================================ " << std::endl;

            if (!gGrid->Cd(sandbox)) {
                continue;
            }

            TGridResult* dirlist = gGrid->Ls(sandbox);
            dirlist->Sort(kTRUE);
            Int_t i =0;
            while (dirlist->GetFileName(i)) {
                printf("%-24s ",dirlist->GetFileName(i++));
                if (!(i%4)) {
                    printf("\n");
                }
            }
            printf("\n");
            if (dirlist)
                delete dirlist;
        }
    }
    std::cout << " ----------LITE_JOB_OPERATIONS-------------------------------------- " << std::endl;
    delete status;
}

//______________________________________________________________________________
Bool_t TJAlienMasterJob::Merge()
{
    return kFALSE;
}

//______________________________________________________________________________
Bool_t TJAlienMasterJob::Merge(const char* inputname,const char* mergeoutput)
{
    TFileMerger merger;

    TJAlienMasterJobStatus* status = (TJAlienMasterJobStatus*)(GetJobStatus());
    TIterator* iter = status->GetJobs()->MakeIterator();

    TObjString* obj = 0;
    while ((obj = (TObjString*)iter->Next()) != 0) {
        TJAlienJobStatus* substatus = (TJAlienJobStatus*)status->GetJobs()->GetValue(obj->GetName());
        TString sandbox;// list sandboxes
        const char* outputdir = substatus->GetJdlKey("OutputDir");
        printf(" Sandbox [%-7s] %s \n", substatus->GetKey("queueId"),sandbox.Data());
        std::cout << " ================================================ " << std::endl;
        if (outputdir) {
            sandbox = outputdir;
        } else {
            sandbox = TString("/proc/") + TString(substatus->GetKey("user")) + TString("/") + TString(substatus->GetKey("queueId")) + TString("/job-output");
        }
        merger.AddFile(TString("alien://")+sandbox+ TString("/") + TString(inputname));
    }

    if (mergeoutput) {
        merger.OutputFile(mergeoutput);
    }

    return merger.Merge();
}
