#include "TJAlienSAXHandler.h"
TJAlienSAXHandler::TJAlienSAXHandler(TJAlienResult *result)
{
	fJAliEnResult = result;
}
void TJAlienSAXHandler::OnStartDocument()
{	  
	Info("TJAlien","start XML document");
}

//SAX Handler
void TJAlienSAXHandler::OnEndDocument()
{
	Info("TJAlien","end XML document");
}

void TJAlienSAXHandler::OnStartElement(const char *name, const TList *attributes)
{
	Info("TJAlienSAXHandler","OnStartElement event: entering");

	// Element name
	TString *startEl = new TString(name);

	TIter next(attributes);
	TXMLAttr *attr;

	TObjString *attrName;
	TObjString *attrValue;

	// Check if the element is the "document" element containing the meta data
	if(!startEl->CompareTo("document")){
		while ((attr = (TXMLAttr*) next())) {
			Info("TJAlienSAXHandler","Attribute name= %s and attribute value=%s",
					attr->GetName(), attr->GetValue());

			attrName = new TObjString(attr->GetName());
			attrValue = new TObjString(attr->GetValue());

			Info("TJAlienSAXHandler", "Adding element to the meta hash");
			fJAliEnResult->SetMetaData(attrName, attrValue);
		}
	}
	// The element is a result element, adding it to TJAlienResult
	else{

		TMap *element = new TMap();

		while ((attr = (TXMLAttr*) next())) {
			Info("TJAlienSAXHandler","Attribute name= %s and attribute value=%s",
					attr->GetName(), attr->GetValue());

			attrName = new TObjString(attr->GetName());
			attrValue = new TObjString(attr->GetValue());

			Info("TJAlienSAXHandler", "Adding element to the JAliEnResult");
			element->Add(attrName, attrValue);
		}

		fJAliEnResult->Add(element);
	}

	delete startEl;

	Info("TJAlienSAXHandler", "OnStartElement event: exiting");

}

void TJAlienSAXHandler::OnEndElement(const char *name)
{
	Info("TJAlien","</ %s >",name);
}

void TJAlienSAXHandler::OnCharacters(const char *characters)
{
	Info("TJAlien","%s",characters);
}

void TJAlienSAXHandler::OnComment(const char *text)
{
	Info("TJAlien","<!--%s -->",text);
}

void TJAlienSAXHandler::OnWarning(const char *text)
{
	Warning("TJAlien","%s",text);
}

void TJAlienSAXHandler::OnError(const char *text)
{
	Error("TJAlien","%s",text);
}

void TJAlienSAXHandler::OnFatalError(const char *text)
{
	Fatal("TJAlien","%s",text);
}

void TJAlienSAXHandler::OnCdataBlock(const char *text, Int_t len)
{
    UNUSED(text);
    UNUSED(len);
	Info("TJAlien","OnCdataBlock()");
}

TJAlienSAXHandler::~TJAlienSAXHandler(){


}


