#include "TJAlienDNSResolver.h"

using std::random_device;

TJAlienDNSResolver::TJAlienDNSResolver(string host, int port) {
    this->host = host;
    this->port = std::to_string(port);
    reset();
}

string TJAlienDNSResolver::addr2string(const struct addrinfo *ai)
{
  char dst[32];
  const struct sockaddr *sa = ai->ai_addr;

  if(sa->sa_family == AF_INET6) {
    sockaddr_in6 const *sin = (sockaddr_in6 const *)ai->ai_addr;
    inet_ntop(sa->sa_family, &sin->sin6_addr, dst, ai->ai_addrlen);
  }
  else if (sa->sa_family == AF_INET) {
    sockaddr_in const *sin = (sockaddr_in const *)ai->ai_addr;
    inet_ntop(sa->sa_family, &sin->sin_addr, dst, ai->ai_addrlen);
  }

  return string(dst);
}

vector<string> TJAlienDNSResolver::get_addr(const char *host, const char *port, int ipv)
{

  vector<string> retval;
  int r = 0;

  struct addrinfo *hints = (struct addrinfo*)malloc(sizeof(struct addrinfo));
  memset(hints, 0, sizeof(struct addrinfo));
  hints->ai_socktype = SOCK_STREAM;

  if(ipv == 4) {
    hints->ai_family = AF_INET;
  } else if (ipv == 6) {
    hints->ai_family = AF_INET6;
  } else {
    hints->ai_family = 0;
  }

  struct addrinfo *ai = NULL;

  r = getaddrinfo(host, port, hints, &ai);

  for(struct addrinfo *ri = ai; ri != NULL; ri = ri->ai_next) {
    retval.push_back(addr2string(ri));
  }

  freeaddrinfo(ai);

  random_device rng;
  shuffle(begin(retval), end(retval), rng);

  return retval;
}

void TJAlienDNSResolver::reset() {
    use_ipv6 = true;
    current_position = 0;

    addr_ipv4 = get_addr(host.c_str(), port.c_str(), 4);
    addr_combined = addr_ipv4;

    if(use_ipv6) {
      addr_ipv6 = get_addr(host.c_str(), port.c_str(), 6);
      addr_combined.insert(addr_combined.end(), addr_ipv6.begin(), addr_ipv6.end());
    }

}

string TJAlienDNSResolver::get_next_host() {
    string result;

    if(current_position >= addr_combined.size()) {
      current_position = 0;
    }

    return addr_combined[current_position++];
}

int TJAlienDNSResolver::lenght() {
    return addr_ipv4.size() + addr_ipv6.size();
}
